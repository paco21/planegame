from Musica import Musica
from Settings import Settings
from Ship import Ship
from Texto import Texto
from Wallpaper import Wallpaper
from game_functions import *


class Game:
    def __init__(self):
        juego.init()
        self.settings = Settings()
        self.screen = juego.display.set_mode((self.settings.screen_width, self.settings.screen_height))
        self.nave = Ship(self.screen, self.settings.nave)
        self.fondo = Wallpaper(self.settings, self.screen)
        self.texto = Texto(self.screen, self.settings)
        self.musicaFondo = Musica("fondo.wav")
        self.musicaDisparo = Musica("disparo.wav")
    def run_game(self):
        self.musicaFondo.play()
        juego.display.set_caption(self.settings.titulo)
        icon = juego.image.load(self.settings.icono)
        juego.display.set_icon(icon)
        self.texto.mostrar_mensaje("PlaneGame", 1000)
        create_fleet(self.settings, self.screen)

        while True:
            update_screen(self.settings, self.screen, self.nave, self.fondo, self.texto)
            check_events(self.nave, self.screen, self.settings, self.musicaDisparo)
